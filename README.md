# ScreenCapture_byClick（极简截屏小工具）

#### 极简截屏小工具
    极简工具：任务栏显示按钮，点击即截屏

#### 使用说明
- 需jre8, 或jdk8环境 （windows默认自带）
- 文本就1个源文件，直接运行或打包成jar运行，都可以。
- 文本直接运行jar文件也可 ScreenCapture_byClick.jar
- windows下的运行方式是：右键~ 先择打开方式 ~用java打开
